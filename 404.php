<? get_header(); ?>

<div class="error">

    <div class="error__wrapper">

        <h1 class="error__title">ups!</h1>

        <p class="error__paragraf">
            Apologies, the page you are looking for does not exist.
        </p>

        <div class="error__wrapper error__wrapper--return-home">

            <a href="/" class="error__return-home-link">
                <span>Return to homepage</span>
            </a>

        </div><!-- ./projects-outro -->

    </div>

</div>

<? get_footer(); ?>
