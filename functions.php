<?php
add_action('after_setup_theme', 'weichie_setup');
function weichie_setup() {
    load_theme_textdomain('weichie', get_template_directory() . '/languages');
    add_theme_support('title-tag');
    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');
    register_nav_menus(array(
        'main-menu-left'             => __('Main Menu Left', 'weichie'),
        'main-menu-right'             => __('Main Menu Right', 'weichie'),
        'footer-menu'           => __('Footer Menu', 'weichie'),
        'lang-menu'           => __('Language Menu', 'weichie'),
    ));
}

add_action('wp_enqueue_scripts', 'weichie_load_scripts');
function weichie_load_scripts() {
    wp_register_style('screen', get_stylesheet_directory_uri().'/css/style.css', '', array(), 'screen');
    wp_enqueue_style('screen');

    // wp_register_style('custom-css', get_stylesheet_directory_uri().'/assets/css/custom.css', '', array(), 'screen');
    // wp_enqueue_style('custom-css');

    wp_enqueue_script("jQuery", "https://code.jquery.com/jquery-3.5.1.min.js", array(), '3.5.1', true );
    // wp_enqueue_script("gsap", "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/gsap.min.js", array(), '3.6.0', true );
    wp_enqueue_script('main', get_stylesheet_directory_uri().'/assets/js/main.js', ['jQuery'], '1.8.1', true);
}

add_filter('the_title', 'weichie_title');
function weichie_title($title) {
    if ($title == '') {
        return '&rarr;';
    } else {
        return $title;
    }
}

function weichie_edit_wp_footer($content) {
    return "<span>Website by <a href='https://weichie.com' target='_blank'>Weichie</a>.</span>";
}
add_filter('admin_footer_text', 'weichie_edit_wp_footer', 11);


add_filter('wp_title', 'weichie_filter_wp_title');
function weichie_filter_wp_title($title) {
    return $title . esc_attr(get_bloginfo('name'));
}

add_action('widgets_init', 'weichie_widgets_init');
function weichie_widgets_init() {
    register_sidebar(array(
        'name' => __('Sidebar Widget Area', 'weichie'),
        'id' => 'primary-widget-area',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => "</li>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));
}

function weichie_custom_pings($comment) {
    $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
    <?php
}

//remove author link in comments
function weichie_remove_comment_author_link( $author_link, $author ) {
  return $author;
}
add_filter( 'get_comment_author_link', 'weichie_remove_comment_author_link', 10, 2 );

add_action( 'admin_init', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}

add_filter('get_comments_number', 'weichie_comments_number');
function weichie_comments_number($count) {
    if (!is_admin()) {
        global $id;
        $comments_by_type =& separate_comments(get_comments('status=approve&post_id=' . $id));
        return count($comments_by_type['comment']);
    } else {
        return $count;
    }
}

add_action('init', 'register_custom_posts_init');
function register_custom_posts_init() {
    register_post_type('Tv', array(
        'labels'             => array(
            'name'               => 'Tv',
            'singular_name'      => 'Tv',
            'menu_name'          => 'Tv'
        ),
        'public'             => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type'    => 'page',
        'hierarchical'       => true,
        'has_archive'        => false,
        'show_in_rest'       => true,
        'rewrite'            => array("with_front" => false),
        'menu_icon'          => 'dashicons-format-video',
        'taxonomies'         => array('category', 'post_tag'),
        'supports'           => array('title', 'excerpt', 'thumbnail', 'page-attributes', 'editor')
    ));
    register_post_type('Team', array(
        'labels'             => array(
            'name'               => 'Team',
            'singular_name'      => 'Team',
            'menu_name'          => 'Team'
        ),
        'public'             => true,
        'capability_type'    => 'page',
        'hierarchical'       => true,
        'has_archive'        => false,
        'show_in_rest'       => true,
        'rewrite'            => array("with_front" => false),
        'menu_icon'          => 'dashicons-groups',
        'taxonomies'         => array('category', 'post_tag'),
        'supports'           => array('title', 'excerpt', 'thumbnail', 'page-attributes', 'editor')
    ));
    register_post_type('Live', array(
        'labels'             => array(
            'name'               => 'Live',
            'singular_name'      => 'Live',
            'menu_name'          => 'Live'
        ),
        'public'             => true,
        'capability_type'    => 'page',
        'hierarchical'       => true,
        'has_archive'        => false,
        'show_in_rest'       => true,
        'rewrite'            => array("with_front" => false),
        'menu_icon'          => 'dashicons-video-alt',
        'taxonomies'         => array('category', 'post_tag'),
        'supports'           => array('title', 'excerpt', 'thumbnail', 'page-attributes', 'editor')
    ));
}



if(function_exists('acf_add_options_page')){
    acf_add_options_page();
}

show_admin_bar(false);

function remove_comments(){
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'remove_comments' );

//filter live cards
function filter_projects() {

    $postType = $_POST['type'];
    $catSlug = $_POST['category'];

    if($catSlug) {

        $ajaxposts = new WP_Query([
            'post_type' => $postType,
            'category_name' => $catSlug,
            'orderby' => 'menu_order', 
            'order' => 'desc',
          ]);
              
    } else {

        $ajaxposts = new WP_Query([
            'post_type' => $postType,
            'orderby' => 'menu_order', 
            'order' => 'desc',
          ]);
    }

    $response = '';
    
  
    if($ajaxposts->have_posts()) {

      while($ajaxposts->have_posts()) : $ajaxposts->the_post();

        $response .= get_template_part('parts/cards-live');

      endwhile;

    } else {

      $response = 'empty';

    }
  
    exit;

  }

  add_action('wp_ajax_filter_projects', 'filter_projects');
  add_action('wp_ajax_nopriv_filter_projects', 'filter_projects');


  
