<?php /* Template Name: Contact */
get_header();if(have_posts()):while(have_posts()):the_post(); ?>

<div class="contact">

        <h2 class="contact__h2">
            <?= the_title(); ?>
        </h2> 
       
        <div class="contact__main-content-wrapper">

            <form action="" class="form" id="contact-form" method="post">

                <div class="form__input-pair-wrapper">
                    <label for="name" class="form__label">vornaam</label>
                    <input type="text" name="name" id="name" class="form__input" placeholder="vornaam">
                </div>

                <div class="form__input-pair-wrapper">
                    <label for="lastname" class="form__label">naam</label>
                    <input type="text" name="lastname" id="lastname" class="form__input" placeholder="naam">
                </div>

                <div class="form__input-pair-wrapper">
                    <label for="gsm" class="form__label">GSM</label>
                    <input type="tel" name="gsm" id="gsm" class="form__input" placeholder="gsm">
                </div>

                <div class="form__input-pair-wrapper">
                    <label for="email" class="form__label">e-mail</label>
                    <input type="email" name="email" id="email" class="form__input" placeholder="e-mail">
                </div>

                <div class="form__input-message-wrapper">
                    <label for="message" class="form__label form__label--message">bericht</label>
                    <textarea type="text" name="message" id="message" class="form__textarea" placeholder="bericht"></textarea>
                </div>

                <input type="submit" value="Verzenden >" class="form__submit">

            </form>

            <div class="message-div" style='display: none;'>

                <h2 class="message-h2">Bedankt voor je bericht!</h2>
                
            </div>

            <section class="address-section" style="background-image: url('<?=get_template_directory_uri();?>/thisisy-files/contact-bg.png');">

                <h3 class="address-section__company-name">
                    <?= the_field('company_name','option'); ?>
                </h3>

                <div class="address-section__address-wrapper">

                    <?= the_field('address','option'); ?>

                    <br>

                    <a href="#" class="address__phonenumber"><?= the_field('phone_number','option'); ?></a>

                </div>

            </section>

        </div>

</div>

<?php endwhile; endif; get_footer(); ?>
