<? /* Template Name: Home */
get_header(); if(have_posts()):while(have_posts()):the_post(); ?>

<div class="home-content-wrapper">

    <!-- hero section -->
    <div class="hero" style="background-image: url('<?=get_the_post_thumbnail_url();?>')">

            <div class="hero__text-intro-wrapper">

                <h2 class="hero__h2"><?= the_title(); ?></h2>
                    
                <?= the_content(); ?>
                
                
            </div>

    </div>

    <form role="search" method="get" id="searchform" action="<?= home_url( '/' ); ?>" >

        <div class="search-form">
            <label class="search-form__label" for="s"><?= __('Search Y for'); ?></label>	
            <input type="hidden" value="post" name="post_type" id="post_type" />
            <input class="search-form__input" type="text" name="s" id="search" value="<?php ' . get_search_query() . ' ?>" placeholder="Search for Y videos here!">
            <button class="search-form__input search-form__input--button" >
                <svg class="w-1 h-1 search-form__svg-icon" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
            </button>
        </div>

    </form>


    <nav class="nav">

        <div class="nav__container">

            <h3 class="nav__h3">Scroll</h3>

            <ul class="nav__ul">

                <li class="nav__li"><a id="sec-1" href="#one">Video</a></li>
                <li class="nav__li">><a id="sec-2" href="#two">Torn</a></li>
                <li class="nav__li">><a id="sec-3" href="#three">Said Pita</a></li>
                <li class="nav__li">><a id="sec-4" href="#four">Fourth</a></li>

            </ul>

        </div>

    </nav>

    <!-- intro text section -->
    <section class="intro-text">

        <p class="intro-text__p">
            <?php the_field('intro_content'); ?>
        </p>

    </section>

    <!-- video section -->
    <section class="video-section scroll-section" id="one" data-id="scroll-1">

        <div class="video-section__video-tumbnail-wrapper">
            <?php $video_image = get_field('video_thumbnail'); ?>
            <?php $video_play_icon = get_field('video_play-icon'); ?>

            <img src="<?= $video_image['url']; ?>" alt="<?= $video_image['alt']; ?>" class="video-section__img-video">
            <img src="<?= $video_play_icon['url']; ?>" alt="<?php $video_play_icon['alt']; ?>" class="video-section__img-play">

        </div>

        <article class="video-section__article ">

            <?php the_field('video_text'); ?>

            <?php $video_link = get_field('video_anchor'); ?>
            
            <a href="<?= $video_link['url']; ?>" class="video-section__anchor" target="<?= $video_link['target']; ?>">

                <?= $video_link['title']; ?>

            </a>

        </article>

    </section>

    <!-- torn part section -->
    <section class="torn-section scroll-section" id="two" data-id="scroll-2" style="background-image: url('<?=get_template_directory_uri();?>/thisisy-files/white-bg.png')">

        <p class="torn-section__p">
            <?php the_field('torn_text'); ?>
        </p>

        <?php $torn_link = get_field('torn_anchor'); ?>

        <a href="<?= $torn_link['url']; ?>" class="torn-section__anchor" target="<?= $torn_link['target']; ?>">

            <?= $torn_link['title']; ?>

        </a>

    </section>

    <!-- said pita section -->
    <section class="pita-section scroll-section" id="three" data-id="scroll-3">

        <article class="pita-section__article">

            <?php the_field('said_text'); ?>

            <!-- <p class="pita-section__p">THIS IS Y is opgericht in 2013 door Said ‘Pita’ Aghassaiy uit Mechelen.</p>
            <p class="pita-section__p">Hij maakte zijn mediadebuut in de jaren 90 met hiphopformatie ABN. </p> -->

        </article>

        <div class="pita-section__img-wrapper">

            <?php $said_img = get_field('said_img'); ?>

            <img src="<?= $said_img['url']; ?>" alt="<?= $said_img['alt']; ?>" class="pita-section__img">

            <h3 class="pita-section__h3">

                <?php the_field('said_img-text'); ?>
                
            </h3>

        </div>

    </section>

    <section class="fourth-section scroll-section" id="four" data-id="scroll-4">

        <h2>Fourth section</h2>

    </section>


</div>

<?php endwhile; endif; get_footer(); ?>
