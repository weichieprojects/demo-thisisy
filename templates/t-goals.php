<?php /* Template Name: Goals */
get_header(); if(have_posts()):while(have_posts()):the_post(); ?>

<div class="goals">

    <div class="goals__heading-wrapper">

        <h2><?= the_title(); ?></h2>

        <?php

            // Check rows exists.
            if( have_rows('goals_icon-repeater') ): ?>

                <ul class="goals__ul">

                    <?php
                    // Loop through rows.
                    while( have_rows('goals_icon-repeater') ) : the_row(); ?>

                        <?php $icon = get_sub_field('goals_icon'); ?>

                        <li class="goals__li">

                            <img src="<?= $icon['url']?>" alt="<?= $icon['alt']?>" class="goals__img">
                            
                            <p class="goals__p">

                                <?php the_sub_field('goals_text'); ?>

                            </p>

                        </li>
                        
                    <?php 
                    // End loop.
                    endwhile; ?>

                </ul>

                    <?php else : ?>

                    <p>Sorry, missing icons and text!</p>
                    
                
            <?php endif; ?>

    </div>

</div>

<?php endwhile; endif; get_footer(); ?>