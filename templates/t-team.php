<?php /* Template Name: Team */
get_header();if(have_posts()):while(have_posts()):the_post(); ?>

<div class="team">
    
    <h2 class="team__h2"><?= the_title(); ?></h2>

    <div class="team-cards">

        <?php 
        $team = new WP_Query([
            'post_type' => 'team'
        ]);

        if($team->have_posts()){

            while($team->have_posts()):$team->the_post();?>

                    <!-- php to store featured image and alt text in variables for use in html -->
                    <?php 

                    $featured_img_id = get_post_thumbnail_id();
                    $featured_img_url = get_the_post_thumbnail_url();

                    if ( $featured_img_id ) {

                        $alt_text = get_post_meta( $featured_img_id, '_wp_attachment_image_alt', true );

                        if ( ! empty( $featured_img_id ) ) {
                            if ( ! empty( $alt_text ) ) {
                                $alt_text = $alt_text;
                            } else {
                                    $alt_text = __( 'no alt text set', 'textdomain' ); 
                                }
                            }
                        }
                    ?>

                    <div class="team-cards__card">

                        <!-- image tag for card cover photo -->
                        <img src="<?= $featured_img_url ?>" alt="<?= $alt_text?>" class="team-cards__img">

                        <!-- php to get the icons (one img and one text) inside the category  -->
                        <?php 
                        
                            // get the current taxonomy term
                            $term = get_the_category();
                            $term_id = $term[0]->term_id;  

                            // vars
                            $image = get_field('category_image', 'category_' . $term_id);
                            $text = get_field('category_text', 'category_' . $term_id);

                        ?>

                        <div class="team-cards__content">

                                <!-- adding icon info via php -->
                                <?php 
                                
                                // I need a check to see if this card has an image or a text for the logo, and output correct variable as well as html accordingly...

                                if ($image) {
                                    ?>

                                    <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>" class="team-cards__logo-img">
                                
                                <?php } elseif ($text) {
                                    $logo = $text; ?>

                                    <h2 class="team-cards__logo-text">
                                        <?php echo $logo ?>
                                    </h2>
                                
                                <?php } else { echo 'no image or text found';} ?>


                                <h2 class="team-cards__title">
                                    <?php the_title();?>
                                </h2>

                                <!-- getting text for card -->
                                
                                <?php $team_card_text = get_field('team_card_text'); ?>

                                <?php 

                                if ($team_card_text) { ?> 

                                    <h3 class="team-cards__card-text">
                                        <?= $team_card_text; ?>
                                    </h3>

                                <?php } else { echo 'no card text found!'; } ?>

                        </div>

                    </div>

            <?php endwhile;
        }?>

    </div>

</div>

<?php endwhile; endif; get_footer(); ?>
