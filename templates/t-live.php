
<?php /* Template Name: Live */
get_header();if(have_posts()):while(have_posts()):the_post(); ?>

<div class="live">

    <div class="live__intro-wrapper">

        <h2 class="live__h2">
            <?= the_title(); ?>

        </h2>
        <div class="filter-date-wrapper">
            <a class="a-filter-date" href="?old=true">Old posts</a>
            
            <a class="a-filter-date" href="?old=false">New posts</a>
        </div>

    </div>

    <?php $categories = get_categories(); ?>

    <ul class="cat-list">

        <li><a class="cat-list_item active" href="#!" data-slug="" data-type="live">All Live videos</a></li>

        <?php foreach($categories as $category) : ?>

            <li>

                <a class="cat-list_item" href="#!" data-slug="<?= $category->slug; ?>" data-type="live">

                    <?= $category->name; ?>

                </a>

            </li>

        <?php endforeach; ?>

    </ul>

    <div class="tv-cards">
        
            

        <?php 
        $today = date('Ymd');

        $dateSorter = '';

        if ($_GET['old'] === 'true') {
            $dateSorter = '<';
        } else {
            $dateSorter = '>';    
        }

        $live = new WP_Query([
            'post_type' => 'live',
            'meta_key'  => 'live_date',
            'orderby'   => 'meta_value_num',
            'order'     => 'ASC',
            'meta_query' => array(
                array(
                    'key'     => 'live_date',
                    'compare' => $dateSorter,
                    'value'   => $today,
                ),

            ),
        ]);

        if($live->have_posts()){

            while($live->have_posts()):$live->the_post();?>

                    <?= get_template_part('parts/cards-live'); ?>

            <?php endwhile;
        }?>

    </div>

</div>

<?php endwhile; endif; get_footer(); ?>

