<?php /* Template Name: Search */
get_header();if(have_posts()):while(have_posts()):the_post(); ?>

<?php

    global $query_string;

    wp_parse_str( $query_string, $search_query );
    $search = new WP_Query( $search_query );

?>

<div class="search">

    <h2 class="search__h2">
                
        <?= the_title(); ?>

    </h2> 

   <!-- <?php get_search_form(); ?> -->


    <div class="search-results-wrapper">

        <?php

            global $wp_query;
            $total_results = $wp_query->found_posts;
            $total_posts = $wp_query->posts;

        ?>  

        <?php
        
            // The Query
            $wp_query = new WP_Query( $args );
            
            // The Loop
            if ( $wp_query->have_posts() ) {
                echo '<ul>';
                while ( $wp_query->have_posts() ) {
                    $wp_query->the_post();
                    echo '<li>' . get_the_title() . '</li>';
                }
                echo '</ul>';
            } else {
                // no posts found
                
            }
            /* Restore original Post Data */
            wp_reset_postdata();

            // ask Bob how I can print informations about my query
            // -- print_r($queried_object);
        ?>


    </div>

</div>

<?php endwhile; endif; get_footer(); ?>
