<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#0a0a0a">
	<link rel="icon" type="image/png" href="https://weichie.com/wp-content/uploads/2019/09/top-logo-ico2.png">
	<link rel="stylesheet" href="https://use.typekit.net/pmw4qda.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Prompt:ital,wght@0,300;0,400;0,600;1,400&display=swap" rel="stylesheet">

	<!-- ANALYTICS -->

	<?php wp_head(); ?>

</head>

<body>

	<header class="header-wordpress">

		<?php wp_nav_menu(array(
			'theme_location'=> 'main-menu-left',
			'container_class' => 'header-wordpress__nav',
			'menu_class' => 'header-menu'

		)); ?>


		<a href="<?= home_url(); ?>" class="header-wordpress__anchor-logo">

			<?php  $logo = get_field('logo_img', 'option');?>

			<img src="<?= $logo['url']; ?>" alt="<?= $logo['alt']; ?>">

		</a>

		<?php wp_nav_menu(array(
			'theme_location'=> 'main-menu-right',
			'container_class' => 'header-wordpress__nav',
			'menu_class' => 'header-menu'

		)); ?>

		<a href="#!" class="hamburger">
			<span></span>
			<span></span>
			<span></span>
		</a>

		<div class="responsive__menu">

			<div class="repsonsive__wrapper">

				<?php wp_nav_menu(array(
					'theme_location'=> 'main-menu-left',
					'container_class' => 'responsive__nav',
					'menu_class' => 'header-menu'

				)); ?>
				<?php wp_nav_menu(array(
					'theme_location'=> 'main-menu-right',
					'container_class' => 'responsive__nav',
					'menu_class' => 'header-menu'

				)); ?>

			</div>
			
		</div>

	</header>

</body>
