<footer class="footer">

	<div class="footer__content-wrapper-left">

		<h2 class="footer__copyright">&copy; 
			<?= the_field('company_name','option'); ?>
		</h2>

	</div>

	<a href="<?= home_url(); ?>" class="footer__anchor-logo">

		<?php  $logo = get_field('logo_img', 'option');?>

		<img src="<?= $logo['url']; ?>" alt="<?= $logo['alt']; ?>">

	</a>

	<?php wp_nav_menu(array(
			'theme_location'=> 'footer-menu',
			'container_class' => 'footer__content-wrapper-right',
			'menu_class' => 'footer-menu'

		)); ?>

</footer>
		
<?php wp_footer(); ?>
</body>
</html>
