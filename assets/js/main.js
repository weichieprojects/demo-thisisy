/*!
 * Bob Weichler - Weichie.com
 * https://weichie.com
 */
$(function(){


	$('#contact-form').on('submit', function(e) {
		e.preventDefault();
		
		$.post("/wp-content/themes/weichiecom/send_mail.php", $(this).serialize(), function (data) {
			if (data.success) {
				$('.contact__h2').addClass('remove-contact-content');
				$('#contact-form,.address-section').hide();
				$('.message-div').fadeIn();
			} else {
				if (data.error == 1) {

					const errors = data.notvalid;
					errors.forEach((ele) => $(`#${ele}`).parent().addClass('error'));

				} else if (data.error == 2) {

					console.log("It looks like there went something wrong with the server. Please try again in a few minutes.");
				
				}
			}
		}, 'json');

	})

	$('.hamburger').on('click', function() {
		$(this).toggleClass('active');
		$('.responsive__menu').fadeToggle();
	})


	//live filters logic

	$('.cat-list_item').on('click', function() {

		$('.cat-list_item').removeClass('active');
		$(this).addClass('active');

		$.ajax({
			type: 'POST',
			url: '/wp-admin/admin-ajax.php',
			dataType: 'html',
			data: {
				action: 'filter_projects',
				category: $(this).data('slug'),
				type: $(this).data('type'),
			},
			
			success: function(res) {
				$('.tv-cards').html(res);
			}
		})
		
	});

	//one page nav challenge on home page
	$(window).on('scroll', function() {
		
		const fromTop = $(window).scrollTop();
		const heroHeight = $('.hero').outerHeight();
			
		if(fromTop >= heroHeight) {
			$('.nav').addClass('fixed');
		} else {
			$('.nav').removeClass('fixed');
		}
		
		$('.scroll-section').each(function() {

			if(fromTop >= $(this).offset().top){

				$('.nav__ul a').removeClass('active');

				$('a[href="#' + $(this).attr('id') + '"]').addClass('active');

			  }

		});
	});

});

/*----------------------------------- */
/* HELPER FUNCTIONS	                 */
/*----------------------------------- */
