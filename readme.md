# Weichie.com Wordpress Starter Theme
The Weichie.com starter wordpress theme


### Used Packages & Libraries

- Barba.js

### Build Setup

``` bash
# install dependencies
$ npm install

# launch gulp
$ gulp watch

```
