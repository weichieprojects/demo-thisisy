<?php get_header(); ?>

<section id="content" class="search__section">


        <div class="result-found">
            <h1 class="result-found__h1"><?php printf(__('Results for: %s', 'weichie'), get_search_query()); ?></h1>
        </div>


        
    <div class="search-results-wrapper">

        <?php
            
            // The Query
            $wp_query = new WP_Query(
                [
                    'post_type' => array('page', 'team', 'tv', 'live', 'pages'),
                    's' => $_GET['s']
                ]
            );
            
            // The Loop
            if ( $wp_query->have_posts() ) {
                echo '<ul class="search-results_ul">';
                while ( $wp_query->have_posts() ) {
                    $wp_query->the_post();
                    echo '<li class="search-results_li">' . get_the_title() . '</li>';
                }
                echo '</ul>';
            } else {
                // no posts found
                ?>
                <article class="no-results">

                    <h2 class="no-results__h2">Sorry<br>...</h2>
    
                    <section class="no-results__try-again">

                        <p class="no-results__try-again-text">No results matched your search.<br>Try again!</p>

                        <?php get_search_form(); ?>

                    </section>

                </article>
                <?php
            }
            /* Restore original Post Data */
            wp_reset_postdata();

        ?>

    </div>


</section>

<?php get_footer(); ?>
