<form role="search" method="get" id="searchform" action="<?= home_url( '/' ); ?>" >

    <div class="search-form">
        <label class="search-form__label" for="s"><?= __('Search Y for'); ?></label>	
        <input type="hidden" value="post" name="post_type" id="post_type" />
        <input class="search-form__input" type="text" name="s" id="search" value="<?php ' . get_search_query() . ' ?>" placeholder="Search for Y videos here!">
        <button class="search-form__input search-form__input--button" >
            <svg class="w-1 h-1 search-form__svg-icon" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
        </button>
    </div>

</form>